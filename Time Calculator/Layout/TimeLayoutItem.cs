﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Time_Calculator
{
    class TimeLayoutItem : LayoutItem
    {
        public enum TimeType
        {
            Add,
            Subtract
        }

        public ComboBox CurrentType;
        public TextBox Hours, Minutes, Seconds;

        public TimeLayoutItem(Point location, int width, Control.ControlCollection p)
            : base(p)
        {
            // Dropdown
            CurrentType = new ComboBox();
            CurrentType.Location = location;
            CurrentType.Width = Globals.TypeWidth;
            CurrentType.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach (TimeType type in Enum.GetValues(typeof(TimeType)))
                CurrentType.Items.Add(type);
            CurrentType.SelectedIndex = 0;

            // Hours
            Hours = new TextBox();
            Hours.Location = new Point(CurrentType.Right + Globals.HorizPadding, location.Y);
            Hours.Width = Globals.TimeWidth;
            Hours.Text = "0";

            // Minutes
            Minutes = new TextBox();
            Minutes.Location = new Point(Hours.Right + Globals.HorizPadding, location.Y);
            Minutes.Width = Globals.TimeWidth;
            Minutes.Text = "0";

            // Seconds
            Seconds = new TextBox();
            Seconds.Location = new Point(Minutes.Right + Globals.HorizPadding, location.Y);
            Seconds.Width = Globals.TimeWidth;
            Seconds.Text = "0";

            Parent.AddRange(new Control[] { CurrentType, Hours, Minutes, Seconds }); // Add all elements to ControlCollection
        }
        /// <summary>
        /// Enable or Disable all elements
        /// </summary>
        /// <param name="enable"></param>
        public override void EnableAll(bool enable)
        {
            CurrentType.Enabled = enable;
            Hours.Enabled = enable;
            Minutes.Enabled = enable;
            Seconds.Enabled = enable;
        }
        /// <summary>
        /// Remove all elements from parent
        /// </summary>
        public override void RemoveAll()
        {
            Parent.Remove(CurrentType);
            Parent.Remove(Hours);
            Parent.Remove(Minutes);
            Parent.Remove(Seconds);
        }
    }
}
