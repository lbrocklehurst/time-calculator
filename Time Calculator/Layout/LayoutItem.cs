﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Time_Calculator
{
    abstract class LayoutItem
    {
        public Control.ControlCollection Parent;

        public LayoutItem(Control.ControlCollection p) { Parent = p; }

        public abstract void EnableAll(bool enable);
        public abstract void RemoveAll();
    }
}
