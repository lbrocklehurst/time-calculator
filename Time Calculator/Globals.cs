﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Calculator
{
    class Globals
    {
        public const int TopOffset = 10, VertPadding = 4, ScrollOffset = 20, HorizPadding = 4,
            TypeWidth = 80;
        public static int TimeWidth
        {
            get;
            set;
        }
    }
}
