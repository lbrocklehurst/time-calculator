﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Time_Calculator
{
    public partial class MainForm : Form
    {
        List<TimeLayoutItem> Times = new List<TimeLayoutItem>(); // List of Time entries

        Label titleType, titleHours, titleMinutes, titleSeconds;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateSize();
            DrawTitles();
            CreateTimeEntry();
            CalculateTotal();
        }

        /// <summary>
        /// Create a Time entry to the list
        /// </summary>
        private void CreateTimeEntry()
        {
            Point location = new Point();
            location.Y = (Times.Count > 0) ? Times[Times.Count - 1].Hours.Bottom + Globals.VertPadding : Globals.VertPadding;

            TimeLayoutItem timelayout = new TimeLayoutItem(location, panelMain.Width, panelMain.Controls);
            Times.Add(timelayout);
            timelayout.CurrentType.SelectedIndexChanged += new System.EventHandler(ValuesChanged);
            timelayout.Hours.TextChanged += new System.EventHandler(ValuesChanged);
            timelayout.Minutes.TextChanged += new System.EventHandler(ValuesChanged);
            timelayout.Seconds.TextChanged += new System.EventHandler(ValuesChanged);
            CalculateTotal();
        }

        /// <summary>
        /// Delete the last Time entry in the list
        /// </summary>
        private void DeleteTimeEntry()
        {
            if (Times.Count <= 0)
                return;
            Times[Times.Count - 1].RemoveAll();
            Times.RemoveAt(Times.Count - 1);
            CalculateTotal();
        }

        private void btnAdd_Click(object sender, EventArgs e) { CreateTimeEntry(); } // Add Time entry
        private void btnDel_Click(object sender, EventArgs e) { DeleteTimeEntry(); } // Delete Time entry
        
        private void ValuesChanged(object sender, EventArgs e) { CalculateTotal(); } // A value has changed, update Total

        /// <summary>
        /// Update size values
        /// </summary>
        private void UpdateSize()
        {
            Globals.TimeWidth = (panelMain.Width - (Globals.TypeWidth + Globals.ScrollOffset + (Globals.HorizPadding * 3))) / 3; // Calculate width for Time input fields
        }

        /// <summary>
        /// Draw the column headings
        /// </summary>
        private void DrawTitles()
        {
            // Type
            titleType = new Label();
            titleType.Text = "Type";
            titleType.Location = new Point(panelMain.Left, Globals.TopOffset);
            titleType.Width = Globals.TypeWidth;

            // Hours
            titleHours = new Label();
            titleHours.Text = "Hours";
            titleHours.Location = new Point(titleType.Left + Globals.TypeWidth + Globals.HorizPadding, Globals.TopOffset);
            titleHours.Width = Globals.TimeWidth;

            // Minutes
            titleMinutes = new Label();
            titleMinutes.Text = "Minutes";
            titleMinutes.Location = new Point(titleHours.Left + Globals.TimeWidth + Globals.HorizPadding, Globals.TopOffset);
            titleMinutes.Width = Globals.TimeWidth;

            // Seconds
            titleSeconds = new Label();
            titleSeconds.Text = "Seconds";
            titleSeconds.Location = new Point(titleMinutes.Left + Globals.TimeWidth + Globals.HorizPadding, Globals.TopOffset);
            titleSeconds.Width = Globals.TimeWidth;

            this.Controls.AddRange(new Control[] { titleType, titleHours, titleMinutes, titleSeconds });
        }

        /// <summary>
        /// Calculate all Times to give Total amount
        /// </summary>
        void CalculateTotal()
        {
            TimeSpan totalTime = new TimeSpan();
            foreach(TimeLayoutItem timelayout in Times)
            {
                int h, m, s;
                int.TryParse(timelayout.Hours.Text, out h);
                int.TryParse(timelayout.Minutes.Text, out m);
                int.TryParse(timelayout.Seconds.Text, out s);
                TimeSpan time = new TimeSpan(h, m, s);
                if(timelayout.CurrentType.SelectedIndex == 0) // Add
                    totalTime += time;
                else if(timelayout.CurrentType.SelectedIndex == 1) // Subtract
                    totalTime -= time;
            }
            txtTotal.Text = totalTime.ToString(); // Output total to TextBox
        }
        
    }
}
